package org.projet.ecoquotas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcoquotasApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcoquotasApplication.class, args);
		
	}

}
